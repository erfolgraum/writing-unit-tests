import { VALID_EMAIL_ENDINGS, regex } from "./email-validator.js";

export function storeToLocalStorage(savedEmail) {
    for (let address of VALID_EMAIL_ENDINGS) {
      if (savedEmail.includes(address) && regex.test(savedEmail)) {  
         return localStorage.setItem("email", savedEmail);
      } 
    }
  
    return false;
  }
