import { expect } from "chai";
import { assert } from "chai";
// import { getLetterCount } from '../email-validator.js'
import {
  validate,
  validateAsync,
  validateWithThrow,
  regexEmailEnding,
  validateWithLog,
} from "../email-validator.js";

describe("Validator first test", () => {
  it("should return 2", () => {
    expect(2).to.equal(2);
  });
});

describe("Testing the validate() function", () => {
  it("Is email @ukr.net not valid", () => {
    const expected = false;
    const actual = validate("hello@ukr.net");
    expect(actual).to.equal(expected);
  });

  it("Is uppercase not allowed", () => {
    const expected = false;
    const actual = validate("HELLO@outlook.com");
    expect(actual).to.equal(expected);
  });

  it("Are digits allowed to use in email", () => {
    const expected = true;
    const actual = validate("12345@gmail.com");
    expect(actual).to.equal(expected);
  });

  it("Are special signs not allowed", () => {
    const expected = false;
    const actual = validate("!na_me&@gmail.com");
    expect(actual).to.equal(expected);
  });
});

//Testing validateAsync(email)
describe("Testing email ending using validateAsync(email) function", () => {
  it("Is a valid email with ending @gmail.com", async () => {
    const expected = true;
    const actual = await validateAsync("user@gmail.com");
    expect(actual).to.equal(expected);
  });

  it("Is uppercase ending allowed or not", async () => {
    const expected = false;
    const actual = await validateAsync("uppercase@GMAIL.COM");
    expect(actual).to.equal(expected);
  });

  it("Is underscore allowed in the email ending", async () => {
    const expected = false;
    const actual = await validateAsync("user@_outlook.com");
    expect(actual).to.equal(expected);
  });

  it("Is it possible to use email ending without dots", async () => {
    const expected = false;
    const actual = await validateAsync("user@yandex");
    expect(actual).to.equal(expected);
  });
});

// Testing validateWithThrow(email)
describe("Testing throw message validateWithThrow(email) function", () => {
  it("throw Error message if the email is not valid", () => {
    let incorrectEmail = "uauser@yahoo.com";

    function isErrorThrown() {
      validateWithThrow(incorrectEmail);
    }
    expect(isErrorThrown).to.throw(`Email ${incorrectEmail} is not valid!`);
  });

  it("Error message is absent", () => {
    let correctEmail = "uauser@gmail.com";
    let expected = true;
    expect(validateWithThrow(correctEmail)).to.equal(expected);
  });
});

// Testing validateWithLog(email) function
describe("Testing validateWithLog(email) function", () => {
  it("Is this func return true", () => {
    let correctEmail = "uauser@gmail.com";
    let expected = true;
    expect(validateWithLog(correctEmail)).to.equal(expected);
  });
});
